package com.hero.portal.activities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.hero.portal.R;
import com.hero.portal.adapters.FileArrayAdapter;
import com.hero.portal.util.Option;
import com.htc.widget.HtcAlertDialog;

@SuppressLint("SdCardPath")
public class ExplorerThemes extends ListActivity {
	FileArrayAdapter adapter;
	private File currentDir;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.IndicatorThemeAlt);
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_in_right, R.anim.fade);
		currentDir = new File("/sdcard/nV/Portal/Themes");
		fill(currentDir);
	}

	private void fill(File f) {
		File[] dirs = f.listFiles();
		List<Option> dir = new ArrayList<Option>();
		List<Option> fls = new ArrayList<Option>();
		try {
			for (File ff : dirs) {
				if (ff.isDirectory())
					dir.add(new Option(ff.getName(), "Folder", ff
							.getAbsolutePath()));
				else {
					fls.add(new Option(ff.getName(), "Location: "
							+ ff.getParent(), ff.getAbsolutePath()));
				}
			}
		} catch (Exception e) {

		}
		Collections.sort(dir);
		Collections.sort(fls);
		dir.addAll(fls);
		if (!f.getName().equalsIgnoreCase("sdcard"))
			adapter = new FileArrayAdapter(ExplorerThemes.this,
					R.layout.file_view, dir);
		this.setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Option o = adapter.getItem(position);
		if (o.getData().equalsIgnoreCase("folder")
				|| o.getData().equalsIgnoreCase("parent directory")) {
			currentDir = new File(o.getPath());
			fill(currentDir);
		} else {
			onFileClick(o, position, id);
		}
	}

	private void onFileClick(final Option o, int position, long id) {
		{
			HtcAlertDialog.Builder aDialog = new HtcAlertDialog.Builder(
					ExplorerThemes.this);
			aDialog.setCancelable(false);
			aDialog.setTitle(R.string.install);
			aDialog.setMessage(R.string.install_message);
			aDialog.setPositiveButton(R.string.yes,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							String[] commands = {
									"adb shell",
									"echo 'boot-recovery ' > /cache/recovery/command",
									"adb shell",
									"echo '--update_package=SDCARD:"
											+ "/nV/Portal/Themes/"
											+ o.getName() + "'"
											+ ">> /cache/recovery/command",
									"adb shell", "reboot recovery" };
							try {
								com.hero.portal.util.RunAsRoot.RunSu(commands);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							((PowerManager) getSystemService(Context.POWER_SERVICE))
									.reboot("recovery");

						}
					});
			aDialog.setNegativeButton(R.string.no,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			aDialog.show();
		}
	}
	
	@Override
	protected void onPause() {
		overridePendingTransition(0, R.anim.slide_out_right);
		super.onPause();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		item.getMenuInfo();
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			overridePendingTransition(0, R.anim.slide_out_right);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
}
