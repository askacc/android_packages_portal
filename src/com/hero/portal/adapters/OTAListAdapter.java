package com.hero.portal.adapters;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hero.portal.R;
import com.hero.portal.util.XMLFormatUtil;

public class OTAListAdapter extends ArrayAdapter<XMLFormatUtil> {
	Activity context;
	List<XMLFormatUtil> news;

	public OTAListAdapter(Activity context, List<XMLFormatUtil> news) {
		super(context, R.layout.page_list_item, news);
		this.context = context;
		this.news = news;
	}

	/* private view holder class */
	private class ViewHolder {
		ImageView imageView;
		TextView txtText;
		TextView txtHeadline;
		TextView txtDate;

	}

	public XMLFormatUtil getItem(int position) {
		return news.get(position);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		LayoutInflater inflater = context.getLayoutInflater();

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.page_list_item, null);
			holder = new ViewHolder();
			holder.txtHeadline = (TextView) convertView
					.findViewById(R.id.headline);
			holder.txtText = (TextView) convertView.findViewById(R.id.text);
			holder.txtDate = (TextView) convertView.findViewById(R.id.date);
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.thumbnail);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		XMLFormatUtil newsitem = (XMLFormatUtil) getItem(position);

		holder.txtHeadline.setText(newsitem.getHeadline());
		holder.txtText.setText(newsitem.getText());
		holder.imageView.setImageBitmap(newsitem.getImageBitmap());
		holder.txtDate.setText(newsitem.getDate());

		return convertView;
	}
}
