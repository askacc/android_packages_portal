package com.hero.portal.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hero.portal.R;
import com.hero.portal.activities.MainActivity;
import com.htc.fragment.app.HtcListFragment;
import com.htc.widget.HtcListView;

public class MenuFragment extends HtcListFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.menu_list, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		CustomAdapter adapter = new CustomAdapter(getActivity());

		adapter.add(new CustomItem("Home", R.drawable.ic_home));
		adapter.add(new CustomItem("RSS Feed", R.drawable.ic_news));
		adapter.add(new CustomItem("ROM Updates", R.drawable.ic_rom_updates));
		adapter.add(new CustomItem("Kernels", R.drawable.ic_kernels));
		adapter.add(new CustomItem("Themes", R.drawable.ic_themes));
		adapter.add(new CustomItem("Icon Packs", R.drawable.ic_icon_packs));
		adapter.add(new CustomItem("Fonts", R.drawable.ic_fonts));

		setListAdapter(adapter);
	}

	private class CustomItem {
		public String tag;
		public int iconRes;

		public CustomItem(String tag, int iconRes) {
			this.tag = tag;
			this.iconRes = iconRes;
		}
	}

	public class CustomAdapter extends ArrayAdapter<CustomItem> {

		public CustomAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(
						R.layout.menu_list_item, null);
			}
			ImageView icon = (ImageView) convertView
					.findViewById(R.id.row_icon);
			icon.setImageResource(getItem(position).iconRes);
			TextView title = (TextView) convertView
					.findViewById(R.id.text1);
			title.setText(getItem(position).tag);

			return convertView;
		}

	}
	
	@Override
	public void onListItemClick(HtcListView lv, View v, int position, long id) {
		Fragment newContent = null;
		switch (position) {
		case 0:
			newContent = new DisplayFragment();
			break;
		case 1:
			newContent = new RSSFragment();
			break;
		case 2:
			newContent = new OTAFragment();
			break;
		case 3:
			newContent = new KernelsFragment();
			break;
		case 4:
			newContent = new ThemesFragment();
			break;
		case 5:
			newContent = new IconPacksFragment();
			break;
		case 6:
			newContent = new FontsFragment();
			break;
		}
		if (newContent != null)
			switchFragment(newContent);
	}

	// the meat of switching the above fragment
	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;

		if (getActivity() instanceof MainActivity) {
			MainActivity fca = (MainActivity) getActivity();
			fca.switchContent(fragment);
		}
	}


}
